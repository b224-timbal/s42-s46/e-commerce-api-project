const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
  firstName: {
    type: String,
    required: [true, "First Name is required!"],
  },
  lastName: {
    type: String,
    required: [true, "Last Name is required!"],
  },
  age: {
    type: Number,
    required: [true, "Age is required!"],
  },
  mobileNo: {
    type: Number,
    required: [true, "Mobile number is required!"],
  },
  email: {
    type: String,
    lowercase: true,
    required: [true, "Email address is required!"],
  },
  password: {
    type: String,
    required: [true, "Password is required"],
  },
  isAdmin: {
    type: Boolean,
    default: false,
  },
  cart: [
    {
      productId: {
        type: String,
        required: [true, "Product ID is required!"],
      },
      productName: {
        type: String,
        required: [false, "Product name is required!"],
      },
      quantity: {
        type: Number,
        default: 1,
      },
      subTotal: {
        type: Number,
        default: 0,
      },
    },
  ],
  orderedProduct: [
    {
      productId: {
        type: String,
        required: [true, "Product ID is required!"],
      },
      productName: {
        type: String,
        required: [false, "Product name is required!"],
      },
      quantity: {
        type: Number,
        default: 1,
      },
      subtotal: {
        type: Number,
        default: 0,
      },
      purchasedOn: {
        type: Date,
        default: new Date(),
      },
    },
  ],
  totalAmount: {
    type: Number,
    default: 0,
  },
});

module.exports = mongoose.model("User", userSchema);
