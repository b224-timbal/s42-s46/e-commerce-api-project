const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({
  name: {
    type: String,
    required: [true, "Product name is required!"],
  },
  description: {
    type: String,
    required: [true, "Product description is required!"],
  },
  price: {
    type: Number,
    required: [true, "Price is required!"],
  },
  isActive: {
    type: Boolean,
    default: true,
  },
  createdOn: {
    type: Date,
    default: new Date(),
  },
  stocks: {
    type: Number,
    required: [true, "Stocks is required!"],
  },
  userOrder: [
    {
      userId: {
        type: String,
        required: [true, "User ID is required"],
      },
      userName: {
        type: String,
        required: [true, "User name is required"],
      },
      orderId: {
        type: String,
        required: [false, "OrderId is required"],
      },
    },
  ],
});

module.exports = mongoose.model("Product", productSchema);
