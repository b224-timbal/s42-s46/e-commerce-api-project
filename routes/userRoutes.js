const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController");
const auth = require("../auth");

// Routes

//User Registration Route
router.post("/register", (req, res) => {
  userController
    .registerUser(req.body)
    .then((resultFromController) => res.send(resultFromController));
});

// User Login Route
router.post("/login", (req, res) => {
  userController
    .loginUser(req.body)
    .then((resultFromController) => res.send(resultFromController));
});

//Order Product route
router.put("/order", auth.verify, (req, res) => {
  const userData = auth.decode(req.headers.authorization);

  if (userData.isAdmin) {
    console.log(`Admin: ${userData.isAdmin}`);
    res.send({ auth: "failed" });
  } else {
    console.log(`isAdmin: ${userData.isAdmin}`);
    let data = {
      productId: req.body.productId,
      quantity: req.body.quantity,
      orderId: req.body.orderId,
    };
    userController
      .orderProduct(userData, data)
      .then((resultFromController) => res.send(resultFromController));
  }
});

//Add to Cart
router.put("/addtoCart", auth.verify, (req, res) => {
  const userData = auth.decode(req.headers.authorization);

  if (userData.isAdmin) {
    res.send({ auth: "failed" });
  } else {
    let data = {
      productId: req.body.productId,
      quantity: req.body.quantity,
    };
    userController
      .addtoCart(userData, data)
      .then((resultFromController) => res.send(resultFromController));
  }
});

//Retrieve user information
router.get("/myProfile", auth.verify, (req, res) => {
  const userData = auth.decode(req.headers.authorization);
  userController
    .getMyProfile({ id: userData.id })
    .then((resultFromController) => res.send(resultFromController));
});

// Retrieve all user information
router.get("/allProfile", auth.verify, (req, res) => {
  const userData = auth.decode(req.headers.authorization);
  if (userData.isAdmin) {
    userController
      .getAllProfile({ id: userData.id })
      .then((resultFromController) => res.send(resultFromController));
  } else {
    res.send({ auth: "failed" });
  }
});

/**************************************************************************************************/
/************************************  STRETCH GOALS **********************************************/
/**************************************************************************************************/

//Assign another admin
router.post("/addAdmin", auth.verify, (req, res) => {
  const userData = auth.decode(req.headers.authorization);
  console.log(`Is admin: ${userData.isAdmin}`);
  if (userData.isAdmin) {
    let data = {
      email: req.body.email,
    };
    userController
      .addAdmin(data)
      .then((resultFromController) => res.send(resultFromController));
  } else {
    res.send({ auth: "failed" });
  }
});

//Remove admin
//Assign another admin
router.post("/removeAdmin", auth.verify, (req, res) => {
  const userData = auth.decode(req.headers.authorization);
  console.log(`Is admin: ${userData.isAdmin}`);
  if (userData.isAdmin) {
    let data = {
      email: req.body.email,
    };
    userController
      .removeAdmin(data, userData)
      .then((resultFromController) => res.send(resultFromController));
  } else {
    res.send({ auth: "failed" });
  }
});

//Retrieve authenticated users order
router.get("/myOrders", auth.verify, (req, res) => {
  const userData = auth.decode(req.headers.authorization);
  if (userData.isAdmin) {
    res.send({ auth: "failed" });
  } else {
    userController
      .getMyOrder({ id: userData.id })
      .then((resultFromController) => res.send(resultFromController));
  }
});

//Retrieve all orders
router.get("/allOrders", auth.verify, (req, res) => {
  const userData = auth.decode(req.headers.authorization);
  if (userData.isAdmin) {
    userController
      .getAllOrder()
      .then((resultFromController) => res.send(resultFromController));
  } else {
    res.send({ auth: "failed" });
  }
});

//Update User Information
router.put("/updateProfile", auth.verify, (req, res) => {
  const userData = auth.decode(req.headers.authorization);
  userController
    .updateMyProfile({ id: userData.id }, req.body)
    .then((resultFromController) => res.send(resultFromController));
});

//Delete Account
router.delete("/deleteAccount", auth.verify, (req, res) => {
  const userData = auth.decode(req.headers.authorization);
  userController
    .deleteMyAccount({ id: userData.id })
    .then((resultFromController) => res.send(resultFromController));
});

module.exports = router;
