const User = require("../models/User");
const bcrypt = require("bcrypt");
const auth = require("../auth");
const Product = require("../models/Product");

//Controller Functions

//User Registration Function
module.exports.registerUser = (reqBody) => {
  return User.find({ email: reqBody.email }).then((result) => {
    if (result.length > 0) {
      return `${reqBody.email} already exists!`;
    } else if (reqBody.age < 18) {
      return `Hi! ${reqBody.firstName} you are not allowed to register. 
			You must be atleast 18 of age above to register.`;
    } else if (reqBody.password == null || reqBody.password.length < 8) {
      return "Password must be atleast 8 alphanumeric characters!";
    } else if (reqBody.password !== reqBody.confirmPassword) {
      return "Password do not match!";
    } else {
      let newUser = new User({
        firstName: reqBody.firstName,
        lastName: reqBody.lastName,
        age: reqBody.age,
        mobileNo: reqBody.mobileNo,
        email: reqBody.email,
        password: bcrypt.hashSync(reqBody.password, 10),
      });
      return newUser.save().then((user, error) => {
        if (error) {
          return false;
        } else {
          return `${reqBody.firstName}, you are successfully registered!`;
        }
      });
    }
  });
};

//User Login
module.exports.loginUser = (reqBody) => {
  return User.findOne({ email: reqBody.email }).then((result) => {
    if (result == null) {
      return `${reqBody.email} does not exist!`;
    } else {
      const isPasswordCorrect = bcrypt.compareSync(
        reqBody.password,
        result.password
      );

      if (isPasswordCorrect) {
        return { access: auth.createAccessToken(result) };
      } else {
        return "Incorrect Password";
      }
    }
  });
};

//OrderProduct
module.exports.orderProduct = (userData, data) => {
  return User.findById(userData.id).then((user) => {
    return Product.findById(data.productId).then((product) => {
      if (data.quantity > product.stocks) {
        return `Insufficient stocks ${product.stocks} stock(s) available!`;
      } else if (product.isActive == false) {
        return `${product.name} is not available!`;
      } else {
        let subtotalAmount = data.quantity * product.price;

        user.orderedProduct.push({
          productId: product.id,
          productName: product.name,
          quantity: data.quantity,
          subtotal: subtotalAmount,
        });

        return user.save().then((user) => {
          product.userOrder.push({
            userId: userData.id,
            userName: `${user.firstName} ${user.lastName}`,
            orderId: data.orderId,
          });

          return product.save().then((product) => {
            let newStocks = product.stocks - data.quantity;
            let updatedStocks = {
              stocks: newStocks,
            };

            return Product.findByIdAndUpdate(
              data.productId,
              updatedStocks
            ).then((result) => {
              if (newStocks == 0) {
                let noStocks = {
                  isActive: false,
                };

                return Product.findByIdAndUpdate(data.productId, noStocks).then(
                  (result) => {
                    return User.aggregate([
                      { $match: { email: userData.email } },
                      { $unwind: "$orderedProduct" },
                      {
                        $group: {
                          _id: "$_id",
                          totalAmount: { $sum: "$orderedProduct.subtotal" },
                        },
                      },
                      { $project: { _id: 0 } },
                    ]).then((data) => {
                      let dataMap = data.map(({ totalAmount }) => totalAmount);
                      let checkTotal = {
                        totalAmount: parseInt(dataMap),
                      };

                      return User.findByIdAndUpdate(
                        userData.id,
                        checkTotal
                      ).then((checkTotal) => {
                        return `
                          You have successfully ordered: 

                          ${product.name}`;
                      });
                    });
                  }
                );
              } else {
                return User.aggregate([
                  { $match: { email: userData.email } },
                  { $unwind: "$orderedProduct" },
                  {
                    $group: {
                      _id: "$_id",
                      totalAmount: { $sum: "$orderedProduct.subtotal" },
                    },
                  },
                  { $project: { _id: 0 } },
                ]).then((data) => {
                  let dataMap = data.map(({ totalAmount }) => totalAmount);
                  let checkTotal = {
                    totalAmount: parseInt(dataMap),
                  };

                  return User.findByIdAndUpdate(userData.id, checkTotal).then(
                    (checkTotal) => {
                      return `
                          You have successfully ordered: 
                          
                          ${product.name}`;
                    }
                  );
                });
              }
            });
          });
        });
      }
    });
  });
};

// Add to cart
module.exports.addtoCart = (userData, data) => {
  return User.findById(userData.id).then((user) => {
    return Product.findById(data.productId).then((product) => {
      if (data.quantity > product.stocks) {
        return `Insufficient stocks ${product.stocks} stock(s) available!`;
      } else if (product.isActive == false) {
        return `${product.name} is not available!`;
      } else {
        let subtotalAmount = data.quantity * product.price;

        user.cart.push({
          productId: product.id,
          productName: product.name,
          quantity: data.quantity,
          subTotal: subtotalAmount,
        });

        return user.save().then((user) => {
          return `${product.name} successfully added to cart! `;
        });
      }
    });
  });
};

//Retrieve user information
module.exports.getMyProfile = (userData) => {
  return User.findById(userData.id).then((result) => {
    result.password = "********";
    return result;
  });
};

// Retrieve all user information
module.exports.getAllProfile = (userData) => {
  return User.find({}).then((result) => {
    result.password = "********";
    return result;
  });
};

/**************************************************************************************************/
/************************************  STRETCH GOALS **********************************************/
/**************************************************************************************************/

//Assign another admin
module.exports.addAdmin = (data) => {
  let updatedRole = {
    isAdmin: true,
  };
  return User.findOneAndUpdate(data, updatedRole).then((updateRole, error) => {
    return updateRole.save().then((result, error) => {
      if (error) {
        return error;
      } else {
        return `You assigned ${result.firstName} as an admin!`;
      }
    });
  });
};

//Remove admin
module.exports.removeAdmin = async (data, userData) => {
  console.log(userData.id);
  console.log(`Data: ${data.email}`);
  return User.findById(userData.id).then((user) => {
    console.log(`User: ${user.email}`);
    if (user.email == data.email) {
      console.log(`Data: ${data}`);
      return "You cannot remove yourself as an admin!";
    } else {
      let updatedRole = {
        isAdmin: false,
      };
      return User.findOneAndUpdate(data, updatedRole).then(
        (updateRole, error) => {
          return updateRole.save().then((result, error) => {
            if (error) {
              return error;
            } else {
              return `You removed ${result.firstName} as an admin!`;
            }
          });
        }
      );
    }
  });
};

//Retrieve authenticated users order
module.exports.getMyOrder = (userData) => {
  return User.findById(userData.id).then((result) => {
    if (result.orderedProduct == null || result.orderedProduct == "") {
      return "Nothing Found";
    } else {
      return `
      Ordered Product: 
      ${result.orderedProduct}

      Total Amount: ${result.totalAmount} PHP

      `;
    }
  });
};

//Retrieve all orders
module.exports.getAllOrder = () => {
  return Product.find({}).then((result) => {
    function getOrder(item) {
      return [item.name, item.userOrder];
    }

    return result.map(getOrder);
  });
};

//Update User Information
module.exports.updateMyProfile = (userData, reqBody) => {
  let updatedUserInfo = {
    firstName: reqBody.firstName,
    lastName: reqBody.lastName,
    age: reqBody.age,
    mobileNo: reqBody.mobileNo,
    email: reqBody.email,
    password: bcrypt.hashSync(reqBody.password, 10),
  };

  return User.findByIdAndUpdate(userData.id, updatedUserInfo).then(
    (result, error) => {
      if (error) {
        return false;
      } else {
        return `
			User info has been updated:

			FROM:
			First Name: ${result.firstName}
			Last Name: ${result.lastName}
			Age: ${result.age}
			Mobile Number: ${result.mobileNo}
			Email:${result.email}
			Password: ********

			TO:
			First Name: ${reqBody.firstName}
			Last Name: ${reqBody.lastName}
			Age: ${reqBody.age}
			Mobile Number: ${reqBody.mobileNo}
			Email:${reqBody.email}
			Password: ********
			`;
      }
    }
  );
};

//Delete Account
module.exports.deleteMyAccount = (userData) => {
  return User.findByIdAndDelete(userData.id).then((result, error) => {
    if (error) {
      return false;
    } else {
      return `
			Account has been deleted:

			First Name: ${result.firstName}
			Last Name: ${result.lastName}
			Age: ${result.age}
			Mobile Number: ${result.mobileNo}
			Email:${result.email}
			Password: ********
			`;
    }
  });
};
